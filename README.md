# xkb-layout-state

_xkblayout-state_ is a small command-line program to get/set the current XKB keyboard layout.

It's a thin wrapper around a slightly version of Jay Bromley's XKeyboard class, the original of which is included in <http://members.dslextreme.com/users/jbromley/files/ruby-xkb.tar.bz2>.

This is the packaged antiX version of the tool, forked from [nonpop/xkblayout-state](https://github.com/nonpop/xkblayout-state) (GPL v.2)

Backend helper for _antiX current keyboard layout_ in antiX control centre.

## Installation

```
$ sudo apt-get update
$ sudo apt-get install xkb-layout-state
```

Hint: If you are installing _antix-current-kb-layout_ package, this package is a dependency.

## Alternatively, Compilation from the sources, on antiX:

- Clone the original source repo to a folder in your local file system by:
```
$ git clone https://github.com/nonpop/xkblayout-state
```

- Make sure x11 development libraries and build essentials are installed:
 ```
$ sudo apt-get install build-essential libx11-dev
```

- Compile object files and link them by:
```
$ make
```
(Command to be executed from within the direcory where _Makefile_ resides.)

- Copy the resulting executable _xkblayout-state_ file to _/usr/local/bin_ folder:
```
$ sudo cp './xkblayout-state' '/usr/local/bin'
```

(Successfully tested on antiX 23.1 _Arditi del Popolo_, 64 and 32 bit full runit)

## Usage

_xkblayout-state_ can be used to either print keyboard layout information on _stdout_ or set the currently active layout.

### Get the current layout(s):
```
$ xkblayout-state print ›format‹
```
This causes the _›format‹_ string to be printed on stdout with the following substitutions:
- _%c_ → current layout number
- _%n_ → current layout name
- _%s_ → current layout symbol
- _%v_ → current layout variant
- _%e_ → current layout variant (equals to %s if %v is empty)
- _%C_ → layout count
- _%N_ → layout names (one per line)
- _%S_ → layout symbols (one per line)
- _%V_ → layout variants (one per line)
- _%E_ → layout variants (one per line; layout symbol is used if variant is empty)
- _%%_ → A literal percent sign: '%'

### Set a new layout:
```
$ xkblayout-state set [+-]›layout_number‹
```
Here _›layout_number‹_ is the number of the layout to be set (starting from 0)
and can be either absolute (default) or relative (if preceded with a plus '+' or minus '-' sign).
If list is exceeded, an end-arround carry is performed.

## License
GPL v.3
